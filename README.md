A podcast catcher written in Go. Inspired by [Podfox](https://github.com/brtmr/podfox).

# Dependencies

You must have `wget` and `ffmpeg` installed.

# Installation

If you have `go` installed and a `GOPATH` configured, then clone the repo and run `go install`:

```sh
git clone https://gitlab.com/lostleonardo/podcat.git
cd podcat
go install
```

## Windows

If you have [scoop](https://scoop.sh) installed, run:

```sh
scoop bucket add https://gitlab.com/lostleonardo/podcat.git
scoop install podcat
```

# Configuration

Add a file called `.podcat.json` to your home directory.

## Linux

```javascript
{
  "max_downloads": 5,
  "podcast_directory": "/home/lostleonardo/podcasts",
  "feeds": [
    {
      "shortname": "the-lunduke-show",
      "title": "The Lunduke Show",
      "kind": "audio",
      "tags": ["computers", "nerd stuff"],
      "url": "http://vault.lunduke.com/LundukeShowMP3.xml"
    }
  ]
}
```

## Windows

```javascript
{
  "max_downloads": 5,
  "podcast_directory": "C:\\Users\\LostLeonardo\\Podcasts",
  "feeds": [
    {
      "shortname": "the-lunduke-show",
      "The Lunduke Show",
      "kind": "audio",
      "tags": ["computers", "nerd stuff"],
      "url": "http://vault.lunduke.com/LundukeShowMP3.xml"
    }
  ]
}
```

# Basic Usage

```
podcat download -shortname <shortname> -max <max-downloads>
podcat episodes -shortname <shortname>
podcat feeds -shortname <shortname>
podcat import -url <feed-url>
podcat rename -shortname <shortname> -newname <new-shortname>
podcat update -shortname <shortname>
```
