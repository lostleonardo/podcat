package feedman

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"sort"
	"strings"
	"sync"
	"time"
)

// unixEpoch is a Unix Epoch constant.
const unixEpoch int64 = -62135596800

// Result is a type for returning data
type Result struct {
	Value string
	Err   error
}

// ANSI terminal colours.
const colorReset string = "\033[0m"
const colorRed string = "\033[31m"
const colorGreen string = "\033[32m"
const colorBlue string = "\033[34m"

// DownloadEpisodes downloads episodes and updates the feed.
func (f *Feed) DownloadEpisodes(ch chan Result, feedPath string, maxDownloads int) {
	var wg sync.WaitGroup
	maxDwn := maxDownloads
	for i, episode := range f.Episodes {
		if maxDwn == 0 {
			break
		}
		if episode.Downloaded == false {
			if maxDwn == maxDownloads {
				ch <- Result{Value: fmt.Sprintf("%s%s%s", string(colorGreen), f.Title, string(colorReset)), Err: nil}
			}
			// Download episode and update ID3 tags
			wg.Add(1)
			go downloadEpisode(&wg, ch, episode, *f, feedPath)

			// Update the Feed reference not the episodes value returned from the `range` command
			f.Episodes[i].Downloaded = true
			f.Episodes[i].File = filename(feedPath, strings.TrimSpace(episode.Name))
			maxDwn--
		}
	}
	wg.Wait()
}

// downloadEpisode downloads an mp3 using 'wget' and saves it to the feed's download
// directory, then it updates the downloaded file's ID3 tags, based on info from the feed.
// Runs in its own goroutine.
func downloadEpisode(wg *sync.WaitGroup, ch chan Result, episode Episode, feed Feed, feedPath string) {
	defer wg.Done()

	episodeName := strings.TrimSpace(episode.Name)
	ch <- Result{Value: fmt.Sprintf(strings.Join([]string{"downloading: ", episodeName}, "")), Err: nil}
	episodeFilename := filename(feedPath, episodeName)
	episodeFilenameTemp := filename(feedPath, fmt.Sprintf("%s_temp", episodeName))
	cmd := exec.Command("wget", "-O", episodeFilenameTemp, episode.URL)
	// cmd.Stdout = os.Stdout
	// cmd.Stderr = os.Stdout
	if err := cmd.Run(); err != nil {
		ch <- Result{Value: "", Err: fmt.Errorf("%s: %v", "could not complete 'wget' request", err)}
	}
	// Update ID3 tags
	// ffmpeg -i file.mp3 -metadata title="Track Title" -metadata artist="Spinal Tap" \
	//-metadata album="Shark Sandwich Shit" out.mp3
	pubDate := time.Unix(episode.Published, 0)
	ffmpegCmd := exec.Command("ffmpeg", "-y", "-i", episodeFilenameTemp,
		"-metadata", fmt.Sprintf("track=%d/%d", episode.Number, len(feed.Episodes)),
		"-metadata", fmt.Sprintf("title=%s", episodeName),
		"-metadata", fmt.Sprintf("album=%s", feed.Title),
		"-metadata", fmt.Sprintf("album_artist=%s", feed.Title),
		"-metadata", fmt.Sprintf("artist=%s", feed.Title),
		"-metadata", fmt.Sprintf("genre=%s", "Podcast"),
		"-metadata", fmt.Sprintf("date=%d", pubDate.Year()),
		"-codec", "copy",
		episodeFilename)
	// ffmpegCmd.Stdout = os.Stdout
	// ffmpegCmd.Stderr = os.Stdout
	if err := ffmpegCmd.Run(); err != nil {
		ch <- Result{Value: "", Err: fmt.Errorf("%s: %v", "could not complete 'ffmpeg' request", err)}
	}
	// Remove temporary file
	err := os.Remove(episodeFilenameTemp)
	if err != nil {
		ch <- Result{Value: "", Err: fmt.Errorf("%s: %v", "could not remove temporary file", err)}
	}
	ch <- Result{Value: fmt.Sprintf(strings.Join([]string{"updating ID3 tags: ", episodeName}, "")), Err: nil}
}

// GetEpisodes gets the episodes from a feed
func (f *Feed) GetEpisodes() []Episode {
	var eps []Episode
	for _, ep := range f.Episodes {
		eps = append(eps, Episode{
			Number:     ep.Number,
			FeedName:   f.Shortname,
			File:       ep.File,
			Name:       ep.Name,
			Published:  ep.Published,
			Downloaded: ep.Downloaded,
			Listened:   ep.Listened,
			URL:        ep.URL,
		})
	}

	return eps
}

// Update updates Feed with the latest episodes.
func (f *Feed) Update(ch chan Result) {
	defer close(ch)

	// Return title
	ch <- Result{Value: fmt.Sprintf("%s%s%s", string(colorGreen), f.Title, string(colorReset)), Err: nil}

	// Create RSS from feed URL
	var rss Feeder
	rss, err := createRSSFeedFromURL(f.URL)
	if err != nil {
		ch <- Result{Value: "", Err: err}
		return
	}

	// If parsing the URL content to RSS fails, attempt
	// to parse the content to an Atom feed instead.
	if rss.title() == "" {
		rss, err = createAtomFeedFromURL(f.URL)
		if err != nil {
			ch <- Result{Value: "", Err: err}
			return
		}
	}

	// Transform data structure
	newFeed := rss.feed(f.URL)

	// Add new episodes to the JSON feed
	for i, episode := range newFeed.Episodes {
		found := false
		titleOrPubDateChanged := false
		for _, oldEpisode := range f.Episodes {
			if episode.URL == oldEpisode.URL && (episode.Published != oldEpisode.Published || episode.Name != oldEpisode.Name) {
				titleOrPubDateChanged = true
			}
			if episode.URL == oldEpisode.URL || episode.Published == oldEpisode.Published || episode.Name == oldEpisode.Name {
				found = true
			}
		}
		if found == false {
			f.Episodes = append(f.Episodes, episode)
			ch <- Result{Value: fmt.Sprintf("New episode."), Err: nil}
		}
		if titleOrPubDateChanged {
			f.Episodes[i].Name = episode.Name
			f.Episodes[i].Published = episode.Published
		}
	}
	sort.Slice(f.Episodes, func(i, j int) bool {
		return f.Episodes[i].Published > f.Episodes[j].Published
	})
	for i := range f.Episodes {
		// Calculate episode number
		epNum := len(f.Episodes) - i
		f.Episodes[i].Number = epNum
	}
}

// DownloadEpisodesFromFeed downloads episodes for the provided feed.
func (c *Config) DownloadEpisodesFromFeed(ch chan Result, shortname string) {
	defer close(ch)

	// Get the configuration file for the feed/channel
	f, err := c.readFeedJSON(shortname)
	if err != nil {
		ch <- Result{Value: "", Err: err}
	}
	feedPath := filepath.Join(c.PodDir, shortname)

	// Download the episodes that have not already been downloaded,
	// using 'wget'
	f.DownloadEpisodes(ch, feedPath, c.MaxDownloads)

	// Save the updated JSON feed data
	err = c.writeFeedJSON(&f, shortname)
	if err != nil {
		ch <- Result{Value: "", Err: err}
	}
}

// FilterFeedOnKind returns a new list of feeds with the kind filter applied
func (c *Config) FilterFeedOnKind(kind string) []Feed {
	var feeds []Feed
	for _, f := range c.Feeds {
		if kind == f.Kind {
			feeds = append(feeds, f)
		}
	}

	return feeds
}

// ImportFeeds checks the config file for feeds that do not have a corresponding download directory
// and the download directory for feeds that do not have a corresponding entry in the config file.
func (c *Config) ImportFeeds() (res []string, err error) {

	// Check if there is a directory that matches each
	// feed shortname
	for _, feed := range c.Feeds {

		// Get all of the files in the podcast directory
		files, err := os.ReadDir(c.PodDir)
		if err != nil {
			return res, fmt.Errorf("could not read podcast directory: %s\n%v", c.PodDir, err)
		}

		// Does the feed shortname match an exisitng podast dir?
		index, match := matchingDir(feed.Shortname, files)

		// If there is no matching directory,
		// import the feed
		if match == false && index == 0 {
			for i, f := range c.Feeds {
				if feed.Shortname == f.Shortname {
					index = i
					break
				}
			}
			r, err := c.ImportFeed(c.Feeds[index].URL, index)
			res = append(res, r)
			if err != nil {
				return res, err
			}
		}
	}

	// Get all of the files in the podcast directory
	files, err := os.ReadDir(c.PodDir)
	if err != nil {
		return res, fmt.Errorf("Could not read podcast directory: %s\n%v", c.PodDir, err)
	}

	// Check if there is a feed shortname that matches each
	// directory
	for _, file := range files {

		// Does the feed shortname match an exisitng podast dir?
		index, match := matchingFeed(file.Name(), c.Feeds)

		// If there is no matching feed, get the details and add it to the feed.json
		if match == false && index == 0 {
			f, err := c.readFeedJSON(file.Name())
			if err != nil {
				return res, err
			}
			newFeed := Feed{Shortname: f.Shortname, Title: f.Title, URL: f.URL, Kind: f.Kind, Tags: f.Tags}
			c.Feeds = append(c.Feeds, newFeed)
			err = c.writeConfigFile()
			if err != nil {
				return res, err
			}
		}
	}

	return res, err
}

// ImportFeed creates a download directory and a feed.json file
// based on an RSS or Atom feed URL.
func (c *Config) ImportFeed(URL string, idx int) (res string, err error) {

	// Create RSS from feed URL
	var rss Feeder
	rss, err = createRSSFeedFromURL(URL)
	if err != nil {
		return res, err
	}

	// If parsing the URL content to RSS fails, attempt
	// to parse the content to an Atom feed instead.
	if rss.title() == "" {
		rss, err = createAtomFeedFromURL(URL)
		if err != nil {
			return res, err
		}
	}

	// If parsing the URL content to Atom fails, attempt
	// to parse the content to an RDF feed instead.
	if rss.title() == "" {
		rss, err = createRDFFeedFromURL(URL)
		if err != nil {
			return res, err
		}
	}

	// Transform from RSS/Atom to a Feed structure
	feed := rss.feed(URL)

	// Calculate feed/channel shortname
	shortname := strings.ToLower(strings.ReplaceAll(rss.title(), " ", "-"))
	shortname = strings.ReplaceAll(shortname, ".", "")
	shortname = strings.ReplaceAll(shortname, ":", "")

	// If the feed was imported from the config file, use that shortname
	if idx > 0 {
		shortname = c.Feeds[idx].Shortname
	}

	// Add shortname, title and URL to feed
	feed.Shortname = shortname
	feed.Title = rss.title()
	feed.URL = URL
	if strings.Contains(URL, "youtube") || strings.Contains(URL, "bitchute") || strings.Contains(URL, "lbry") {
		feed.Kind = "video"
	} else {
		feed.Kind = "audio"
	}

	// Create channel folder
	feedPath := filepath.Join(c.PodDir, shortname)
	err = os.Mkdir(feedPath, 0755)
	if err != nil {
		return res, fmt.Errorf("could not create channel directory: %s\n%v", feedPath, err)
	}

	// Save JSON feed data to configuration file
	err = c.writeFeedJSON(&feed, shortname)
	if err != nil {
		return res, err
	}

	// Find the feed in the config and update it
	found := false
	for _, feed := range c.Feeds {
		if feed.Shortname == shortname {
			found = true
			break
		}
	}
	if found == false {
		f := Feed{Shortname: shortname, Title: rss.title(), URL: URL, Kind: feed.Kind, Tags: feed.Tags}
		c.Feeds = append(c.Feeds, f)
	}
	err = c.writeConfigFile()
	if err != nil {
		return res, err
	}

	return fmt.Sprintf("Imported: %s", shortname), nil
}

// GetEpisodeByNumber gets the episode for the provided feed based on number
func (c *Config) GetEpisodeByNumber(shortname string, n int) (ep Episode, err error) {
	var episode Episode
	f, err := c.readFeedJSON(shortname)
	if err != nil {
		return ep, err
	}
	for _, ep := range f.GetEpisodes() {
		if ep.Number == n {
			episode = ep
		}
	}
	return episode, nil
}

// GetEpisodesForFeed gets the episodes for the provided feed
func (c *Config) GetEpisodesForFeed(shortname string) (eps []Episode, err error) {
	f, err := c.readFeedJSON(shortname)
	if err != nil {
		return eps, err
	}
	return f.GetEpisodes(), nil
}

// SelectEpisodeByNumber returns an episode from an episode list
func (c *Config) SelectEpisodeByNumber(shortname string, epNumber int) []Episode {
	ep, err := c.GetEpisodeByNumber(shortname, epNumber)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	var eps []Episode
	eps = append(eps, ep)

	return eps
}

// SelectRandomFeed returns a random feed from the provided feed list, and returns it
// inside of a single element list
func (c *Config) SelectRandomFeed(providedFeeds []Feed) []Feed {
	var feeds []Feed
	rand.New(rand.NewSource(1))
	i := rand.Intn(len(providedFeeds))
	randomFeed := providedFeeds[i]
	feeds = append(feeds, randomFeed)

	return feeds
}

// SetEpisodeAsListened increments the listened count for the provided episode
func (c *Config) SetEpisodeAsListened(shortname string, number int) error {
	f, err := c.readFeedJSON(shortname)
	if err != nil {
		return err
	}
	for i, ep := range f.Episodes {
		if ep.Number == number {
			f.Episodes[i].Listened++
		}
	}
	err = c.writeFeedJSON(&f, shortname)
	if err != nil {
		return err
	}

	return nil
}

// SetEpisodeAsNotListened decrements the listened count for the provided episode
func (c *Config) SetEpisodeAsNotListened(shortname string, number int) error {
	f, err := c.readFeedJSON(shortname)
	if err != nil {
		return err
	}
	for i, ep := range f.Episodes {
		if ep.Number == number {
			f.Episodes[i].Listened--
		}
	}
	err = c.writeFeedJSON(&f, shortname)
	if err != nil {
		return err
	}

	return nil
}

// Rename renames the provided feed's shortname and download directory.
func (c *Config) Rename(shortname, newName string) error {
	f, err := c.readFeedJSON(shortname)
	if err != nil {
		return err
	}

	// Get all of the files in the podcast directory
	files, err := os.ReadDir(c.PodDir)
	if err != nil {
		return fmt.Errorf("could not read podcast directory: %s\n%v", c.PodDir, err)
	}

	// Find the matching folder
	for _, file := range files {
		if f.Shortname == file.Name() {
			feedPath := filepath.Join(c.PodDir, f.Shortname)
			newFeedPath := filepath.Join(c.PodDir, newName)
			// Update the feed JSON - you have to update the feed first
			// becuase it relies upon finding the file based on the curShortname
			f.Shortname = newName
			err = c.writeFeedJSON(&f, shortname)
			if err != nil {
				return err
			}

			// Find the feed in the config and update it
			for i, feed := range c.Feeds {
				if feed.Shortname == shortname {
					c.Feeds[i].Shortname = newName
				}
			}
			err = c.writeConfigFile()
			if err != nil {
				return err
			}

			// Rename the folder
			err = os.Rename(feedPath, newFeedPath)
			if err != nil {
				return fmt.Errorf("could not rename the directory: %s\n%v", feedPath, err)
			}
		}
	}

	return nil
}

// RenameTitle renames the provided feed's title
func (c *Config) RenameTitle(shortname, newTitle string) error {
	f, err := c.readFeedJSON(shortname)
	if err != nil {
		return err
	}

	// Update the feed JSON
	f.Title = newTitle
	err = c.writeFeedJSON(&f, shortname)
	if err != nil {
		return err
	}

	// Find the feed in the config and update it
	for i, feed := range c.Feeds {
		if feed.Shortname == shortname {
			c.Feeds[i].Title = newTitle
		}
	}
	err = c.writeConfigFile()
	if err != nil {
		return err
	}

	return nil
}

// UpdateFeed updates the feed.json for all feeds.
func (c *Config) UpdateFeed(ch chan Result, shortname string) {
	defer close(ch)

	// Get the configuration file for the feed/channel
	f, err := c.readFeedJSON(shortname)
	if err != nil || f.Shortname != shortname {
		ch <- Result{Value: "", Err: err}
	}

	// Update feed
	uch := make(chan Result)
	go f.Update(uch)
	for r := range uch {
		ch <- r
	}
	if err != nil {
		ch <- Result{Value: "", Err: err}
	}

	// Save the updated JSON data
	if err := c.writeFeedJSON(&f, shortname); err != nil {
		ch <- Result{Value: "", Err: err}
	}
}

// writeConfigFile writes out the .podcat.json config file.
func (c *Config) writeConfigFile() error {

	// Get user's home directory
	user, err := user.Current()
	if err != nil {
		return fmt.Errorf("could not find user's home directory: %v", err)
	}

	// Write config file
	jsonData, _ := json.MarshalIndent(c, "", "    ")
	filePath := filepath.Join(user.HomeDir, ".podcat.json")
	err = os.WriteFile(filePath, jsonData, 0755)
	if err != nil {
		return fmt.Errorf("could not write to file: %s\n%v", filePath, err)
	}

	return nil
}

// FilterEpisodesOnListened returns the episodes with a listen count greater than 0
func FilterEpisodesOnListened(eps []Episode) []Episode {
	var filterEps []Episode
	for _, ep := range eps {
		if ep.Listened > 0 {
			filterEps = append(filterEps, ep)
		}
	}

	return filterEps
}

// FilterEpisodesOnNotListened returns the episodes with a listen count greater than 0
func FilterEpisodesOnNotListened(eps []Episode) []Episode {
	var filterEps []Episode
	for _, ep := range eps {
		if ep.Listened == 0 {
			filterEps = append(filterEps, ep)
		}
	}

	return filterEps
}

// GetDownloadedEpisodes returns the downloaded episodes in a list of episodes
func GetDownloadedEpisodes(eps []Episode) []Episode {
	var filterEps []Episode
	for _, ep := range eps {
		if ep.Downloaded {
			filterEps = append(filterEps, ep)
		}
	}

	return filterEps
}

// GetFirstEpisode returns the first episode from a list of episodes
func GetFirstEpisode(eps []Episode) []Episode {
	var filterEps []Episode
	// Order ascending
	sort.Slice(eps, func(i, j int) bool {
		return eps[i].Published < eps[j].Published
	})
	// Add oldest episode
	filterEps = append(filterEps, eps[0])

	return filterEps
}

// GetLatestEpisode returns the latest episode from a list of episodes
func GetLatestEpisode(eps []Episode) []Episode {
	var filterEps []Episode

	// Order descending
	sort.Slice(eps, func(i, j int) bool {
		return eps[i].Published > eps[j].Published
	})
	filterEps = append(filterEps, eps[0])

	return filterEps
}

// GetNextEpisode returns the next unplayed episode from a list of episodes
func GetNextEpisode(eps []Episode) []Episode {
	var filterEps []Episode

	// Order ascending
	sort.Slice(eps, func(i, j int) bool {
		return eps[i].Published < eps[j].Published
	})
	// Add oldest unplayed episode
	for i, ep := range eps {
		if ep.Listened == 0 {
			filterEps = append(filterEps, eps[i])
			break
		}
	}

	return filterEps
}

// GetNotDownloadedEpisodes returns the not downloaded episodes in a list of episodes
func GetNotDownloadedEpisodes(eps []Episode) []Episode {
	var filterEps []Episode
	for _, ep := range eps {
		if !ep.Downloaded {
			filterEps = append(filterEps, ep)
		}
	}

	return filterEps
}

// SelectRandomEpisode returns a random episode from an episode list
func SelectRandomEpisode(eps []Episode) []Episode {
	var randomEps []Episode

	rand.New(rand.NewSource(1))
	i := rand.Intn(len(eps))
	randomEp := eps[i]
	randomEps = append(randomEps, randomEp)

	return randomEps
}

// createRSSFeedFromURL creates an RSS structure from a feed URL.
func createRSSFeedFromURL(feedURL string) (rss RSS, err error) {
	resp, err := http.Get(feedURL)
	if err != nil {
		return rss, fmt.Errorf("could not resolve URL: %s\n%v", feedURL, err)
	}
	if resp.StatusCode >= 500 {
		return rss, fmt.Errorf("Response code: %s", resp.Status)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return rss, fmt.Errorf("couldn't read HTTP response body: %v", err)
	}
	if string(body) == "" {
		return rss, fmt.Errorf("body is empty")
	}

	// Remove the XML declaration
	body = body[strings.Index(string(body), "rss")-1 : len(body)-1]

	// Parse XML
	xml.Unmarshal(body, &rss)
	for i := range rss.Channel.Episodes {
		// mark bitchute channel entries as downloadd by default.
		if strings.Contains(rss.Channel.Link, "bitchute") {
			rss.Channel.Episodes[i].Downloaded = true
		}
	}

	return rss, nil
}

// createAtomFeedFromURL creates an Atom structure from a feed URL.
func createAtomFeedFromURL(feedURL string) (a Atom, err error) {
	resp, err := http.Get(feedURL)
	if err != nil {
		return a, fmt.Errorf("could not resolve URL: %v", err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return a, fmt.Errorf("couldn't read HTTP response body: %v", err)
	}

	// Parse XML
	xml.Unmarshal(body, &a)
	for i := range a.Entries {
		// mark youtube channel entries as downloadd by default.
		if strings.Contains(a.Link.URL, "youtube") {
			a.Entries[i].Downloaded = true
		}
	}

	return a, nil
}

// createRDFFeedFromURL creates an RDF structure from a feed URL.
func createRDFFeedFromURL(URL string) (r RDF, err error) {
	resp, err := http.Get(URL)
	if err != nil {
		return r, fmt.Errorf("could not resolve URL: %v", err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return r, fmt.Errorf("couldn't read HTTP response body: %v", err)
	}

	// Remove the XML declaration
	body = body[strings.Index(string(body), "rdf")-1 : len(body)-1]

	// Parse XML
	xml.Unmarshal(body, &r)
	for i := range r.Episodes {
		// mark mit channel entries as downloadd by default.
		if strings.Contains(r.Channel.Title, "mit") {
			r.Episodes[i].Downloaded = true
		}
	}

	return r, nil
}

// feed transforms an RSS type to a Feed type.
func (r RSS) feed(URL string) Feed {
	var eps []Episode
	for _, v := range r.Channel.Episodes {

		// Calculate publication time
		var unixTime int64
		for _, layout := range dateTimelayouts {
			t, _ := time.Parse(layout, v.Published)
			unixTime = t.Unix()
			// If the unix time does note equal the unix epoch then
			// break out of the loop, otherwise try to parse the date
			// based on a different date time layout
			if unixTime != unixEpoch {
				break
			}
		}
		e := Episode{
			Name:       v.Name,
			URL:        v.Enclosure.URL,
			Published:  unixTime,
			Downloaded: v.Downloaded,
			Listened:   v.Listened,
		}
		// Bitchute and Odysee put links to episodes in the link
		// rather than in the enclosure element.
		if strings.Contains(URL, "bitchute") || strings.Contains(URL, "lbryfeed") {
			e.URL = v.Link
			e.Downloaded = true
		}
		eps = append(eps, e)
	}
	sort.Slice(eps, func(i, j int) bool {
		return eps[i].Published > eps[j].Published
	})
	for i := range eps {
		// Calculate episode number
		epNum := len(eps) - i
		eps[i].Number = epNum
	}
	f := Feed{Episodes: eps}

	return f
}

// feed transforms an Atom type to a Feed type.
func (a Atom) feed(URL string) Feed {
	var eps []Episode
	for _, v := range a.Entries {

		// Calculate publication time
		var unixTime int64
		for _, layout := range dateTimelayouts {
			t, _ := time.Parse(layout, v.Published)
			unixTime = t.Unix()
			// If the unix time does note equal the unix epoch then
			// break out of the loop, otherwise try to parse the date
			// based on a different date time layout
			if unixTime != unixEpoch {
				break
			}
		}
		e := Episode{
			Name:       v.Name,
			URL:        v.Link.URL,
			Published:  unixTime,
			Downloaded: v.Downloaded,
			Listened:   v.Listened,
		}
		eps = append(eps, e)
	}
	sort.Slice(eps, func(i, j int) bool {
		return eps[i].Published > eps[j].Published
	})
	for i := range eps {
		// Calculate episode number
		epNum := len(eps) - i
		eps[i].Number = epNum
	}
	f := Feed{Episodes: eps}

	return f
}

// feed transforms an RDF type to a Feed type.
func (r RDF) feed(URL string) Feed {
	var eps []Episode
	for _, v := range r.Episodes {

		// Calculate publication time
		var unixTime int64
		for _, layout := range dateTimelayouts {
			t, _ := time.Parse(layout, v.Published)
			unixTime = t.Unix()
			// If the unix time does note equal the unix epoch then
			// break out of the loop, otherwise try to parse the date
			// based on a different date time layout
			if unixTime != unixEpoch {
				break
			}
		}
		e := Episode{
			Name:       v.Name,
			URL:        v.Link,
			Published:  unixTime,
			Downloaded: v.Downloaded,
			Listened:   v.Listened,
		}
		eps = append(eps, e)
	}
	sort.Slice(eps, func(i, j int) bool {
		return eps[i].Published > eps[j].Published
	})
	for i := range eps {
		// Calculate episode number
		epNum := len(eps) - i
		eps[i].Number = epNum
	}
	f := Feed{Episodes: eps}

	return f
}

// filename cleans filenames (found in URLs) of characters that are not normally allowed in filenames.
func filename(pathname, episodeName string) string {
	episodeFilename := filepath.Join(pathname, fmt.Sprintf("%s%s", strings.ReplaceAll(episodeName, "/", "|"), ".mp3"))
	episodeFilename = strings.ReplaceAll(episodeFilename, "?", ".")
	return episodeFilename
}

// matchingDir checks whether a filename exists within a given directory.
func matchingDir(shortname string, files []os.DirEntry) (idx int, match bool) {
	for idx, dir := range files {
		if dir.Name() == shortname {
			return idx, true
		}
	}
	return idx, false
}

// matchingFeed checks whether a feed shortname matches a podcast directory.
func matchingFeed(shortname string, feeds []Feed) (idx int, match bool) {
	for idx, feed := range feeds {
		if feed.Shortname == shortname {
			return idx, true
		}
	}
	return idx, false
}

// readFeedJSON retrieves the feed.json for the given feed.
func (c *Config) readFeedJSON(shortname string) (feed Feed, err error) {
	JSONPath := filepath.Join(c.PodDir, shortname, "feed.json")
	dat, err := os.ReadFile(JSONPath)
	if err != nil {
		return feed, fmt.Errorf("Could not read feed.json configuration file for %s. Did you mistype something?", shortname)
	}

	json.Unmarshal(dat, &feed)

	return feed, nil
}

// writeFeedJSON saves the feed.json for the given feed.
func (c *Config) writeFeedJSON(feed *Feed, shortname string) error {
	feedPath := filepath.Join(c.PodDir, shortname, "feed.json")
	jsonData, _ := json.MarshalIndent(feed, "", "    ")
	if len(feed.Episodes) == 0 {
		return fmt.Errorf("no JSON data to write: %s", shortname)
	}
	if len(jsonData) == 0 {
		return fmt.Errorf("no JSON data to write: %s", shortname)
	}
	err := os.WriteFile(feedPath, jsonData, 0755)
	if err != nil {
		return fmt.Errorf("could not write to file: %s\n%v", feedPath, err)
	}

	return nil
}

func (a Atom) title() string {
	return a.Title
}

func (r RSS) title() string {
	return r.Channel.Title
}

func (r RDF) title() string {
	return r.Channel.Title
}

// Feeder is an object that can be transformed into a Feed.
type Feeder interface {
	feed(URL string) Feed
	title() string
}

// Config JSON configuration
type Config struct {
	MaxDownloads int    `json:"max_downloads"`
	PodDir       string `json:"podcast_directory"`
	Feeds        []Feed `json:"feeds"`
}

// RSS RSS feed for a podcast channel
type RSS struct {
	XMLName xml.Name `xml:"rss" json:"-"`
	Channel Channel  `xml:"channel" json:"channel"`
}

// Channel podcast channel
type Channel struct {
	XMLName     xml.Name     `xml:"channel" json:"-"`
	Title       string       `xml:"title" json:"title"`
	Link        string       `xml:"link" json:"-"`
	Description string       `xml:"description" json:"-"`
	Language    string       `xml:"language" json:"-"`
	LastBuild   string       `xml:"lastBuildDate" json:"-"`
	Episodes    []XMLEpisode `xml:"item" json:"episodes,omitempty"`
}

// XMLEpisode podcast episode
type XMLEpisode struct {
	XMLName     xml.Name  `xml:"item" json:"-"`
	Name        string    `xml:"title" json:"name"`
	Enclosure   Enclosure `xml:"enclosure" json:"enclosure"`
	Published   string    `xml:"pubDate" json:"published"`
	GUID        string    `xml:"guid" json:"-"`
	Description string    `xml:"description" json:"-"`
	Link        string    `xml:"link" json:"-"`
	Downloaded  bool      `json:"downloaded"`
	Listened    uint32    `json:"listened"`
}

// RDF RFD feed
type RDF struct {
	XMLName  xml.Name     `xml:"RDF" json:"-"`
	Channel  RDFChannel   `xml:"channel" json:"channel"`
	Episodes []RDFEpisode `xml:"item" json:"episodes,omitempty"`
}

// RDFChannel podcast channel
type RDFChannel struct {
	XMLName xml.Name `xml:"channel" json:"-"`
	Title   string   `xml:"title" json:"title"`
	Link    string   `xml:"link" json:"-"`
	// Description string   `xml:"description" json:"-"`
}

// RDFEpisode podcast episode
type RDFEpisode struct {
	XMLName     xml.Name `xml:"item" json:"-"`
	Name        string   `xml:"title" json:"name"`
	Published   string   `xml:"date" json:"published"`
	Description string   `xml:"description" json:"-"`
	Link        string   `xml:"link" json:"-"`
	Downloaded  bool     `json:"downloaded"`
	Listened    uint32   `json:"listened"`
}

// Enclosure an XML element containing a link to a podcast episode
type Enclosure struct {
	XMLName xml.Name `xml:"enclosure" json:"-"`
	URL     string   `xml:"url,attr" json:"url"`
}

// Feed podcast feed
type Feed struct {
	Episodes  []Episode `json:"episodes,omitempty"`
	Shortname string    `json:"shortname"`
	Title     string    `json:"title"`
	URL       string    `json:"url"`
	Kind      string    `json:"kind"`
	Tags      []string  `json:"tags"`
}

// Episode podcast episode
type Episode struct {
	Number     int    `json:"no"`
	Name       string `json:"name"`
	URL        string `json:"url"`
	File       string `json:"file"`
	Published  int64  `json:"published"`
	Downloaded bool   `json:"downloaded"`
	Listened   uint32 `json:"listened"`
	FeedName   string `json:"-"`
}

// Atom youtube feed
type Atom struct {
	XMLName xml.Name    `xml:"feed"`
	Link    AtomLink    `xml:"link"`
	Title   string      `xml:"title"`
	Entries []AtomEntry `xml:"entry"`
}

// AtomEntry youtube entry
type AtomEntry struct {
	XMLName    xml.Name `xml:"entry"`
	Name       string   `xml:"title"`
	Link       AtomLink `xml:"link"`
	Published  string   `xml:"published"`
	Downloaded bool     `json:"downloaded"`
	Listened   uint32   `json:"listened"`
}

// AtomLink youtube link
type AtomLink struct {
	XMLName xml.Name `xml:"link"`
	URL     string   `xml:"href,attr"`
}

var dateTimelayouts = []string{
	"Mon, 02 Jan 2006 15:04:05 -0700",
	"Mon, 2 Jan 2006 15:04:05 -0700",
	"Mon, 02 Jan 2006 15:04:05 MST",
	"Mon, 2 Jan 2006 15:04:05 MST",
	"Mon, 02 Jan 2006 15:04 MST",
	"Mon, 2 Jan 2006 15:04 MST",
	"2006-01-02T15:04:05-07:00",
}
