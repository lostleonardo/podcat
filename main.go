package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"sort"
	"strings"
	"text/tabwriter"
	"time"

	"gitlab.com/lostleonardo/podcat/pkg/feedman"
)

func main() {
	// Subcommands
	commandDownload := flag.NewFlagSet("download", flag.ExitOnError)
	commandEpisodes := flag.NewFlagSet("episodes", flag.ExitOnError)
	commandFeeds := flag.NewFlagSet("feeds", flag.ExitOnError)
	commandImport := flag.NewFlagSet("import", flag.ExitOnError)
	commandRename := flag.NewFlagSet("rename", flag.ExitOnError)
	commandUpdate := flag.NewFlagSet("update", flag.ExitOnError)

	// Subcommand flag input values
	var epAsc bool
	var epDesc bool
	var epDownloaded bool
	var epFile bool
	var epFirst bool
	var epKind string
	var epLatest bool
	var epListened bool
	var epNext bool
	var epNotDownloaded bool
	var epNotListened bool
	var epNotPlayed bool
	var epNumber int
	var epNumbers bool
	var epPlayed bool
	var epRandom bool
	var epTag string
	var epThisWeek bool
	var epToday bool
	var epURL bool
	var feedname bool
	var feedKind string
	var feedShortname bool
	var feedRandom bool
	var feedURL bool
	var importURL string
	var newname string
	var maxDownloads int
	var shortname string
	var newTitle string

	// Subcommand flags
	commandImport.StringVar(&importURL, "url", "", "feed URL (required)")
	commandDownload.StringVar(&shortname, "shortname", "", "shortname of a given feed (optional)")
	commandDownload.IntVar(&maxDownloads, "max", 0, "maximum number of concurrent downloads (optional)")
	commandEpisodes.StringVar(&shortname, "shortname", "", "shortname of a given feed (required)")
	commandEpisodes.BoolVar(&epAsc, "asc", false, "order episodes ascending (optional)")
	commandEpisodes.BoolVar(&epDesc, "desc", false, "order episodes descending (optional)")
	commandEpisodes.BoolVar(&epDownloaded, "downloaded", false, "display downloaded episodes (optional)")
	commandEpisodes.BoolVar(&epFile, "file", false, "display files associated with episodes (optional)")
	commandEpisodes.BoolVar(&epFirst, "first", false, "select first episode (optional)")
	commandEpisodes.StringVar(&epKind, "kind", "", "display episodes from feeds of a given kind (optional)")
	commandEpisodes.BoolVar(&epLatest, "latest", false, "select latest episode (optional)")
	commandEpisodes.BoolVar(&epListened, "listened", false, "display episodes with a listened count of more than one (optional)")
	commandEpisodes.BoolVar(&epNext, "next", false, "select next unplayed episode (optional)")
	commandEpisodes.BoolVar(&epNotDownloaded, "not-downloaded", false, "display not downloaded episodes (optional)")
	commandEpisodes.BoolVar(&epNotListened, "not-listened", false, "display episodes with a listened count of zero (optional)")
	commandEpisodes.BoolVar(&epNotPlayed, "not-played", false, "mark an episode as not played (optional)")
	commandEpisodes.IntVar(&epNumber, "number", 0, "episode number within a given feed (optional)")
	commandEpisodes.BoolVar(&epNumbers, "numbers", false, "display episodes with episode numbers (optional)")
	commandEpisodes.BoolVar(&epPlayed, "played", false, "mark an episode as played (optional)")
	commandEpisodes.BoolVar(&epRandom, "random", false, "select random episode (optional)")
	commandEpisodes.StringVar(&epTag, "tag", "", "display episodes from feeds with a given tag (optional)")
	commandEpisodes.BoolVar(&epThisWeek, "this-week", false, "display episodes published this week (optional)")
	commandEpisodes.BoolVar(&epToday, "today", false, "display episodes published on this day (optional)")
	commandEpisodes.BoolVar(&epURL, "url", false, "display URLs associated with episodes (optional)")
	commandEpisodes.BoolVar(&feedname, "feedname", false, "display episodes, including feedname (optional)")
	commandFeeds.StringVar(&feedKind, "kind", "", "display feeds based on kind (optional)")
	commandFeeds.BoolVar(&feedShortname, "shortname", false, "display only feed shortname (optional)")
	commandFeeds.BoolVar(&feedRandom, "random", false, "select random feed (optional)")
	commandFeeds.BoolVar(&feedURL, "url", false, "display only feed URL (optional)")
	commandRename.StringVar(&shortname, "shortname", "", "current shortname for a given feed (optional)")
	commandRename.StringVar(&newname, "newname", "", "new shortname for a given feed (required)")
	commandRename.StringVar(&newTitle, "newtitle", "", "new title for a given feed (required)")
	commandUpdate.StringVar(&shortname, "shortname", "", "shortname for a given feed (optional)")

	// Usage info
	flag.Usage = func() {
		fmt.Printf("Usage of podcat:\n")
		fmt.Printf("  download -shortname string -max int\n")
		fmt.Printf("        downloads episodes\n")
		fmt.Printf("  episodes -asc -desc -downloaded -feedname -file -first -kind -latest -listened -next -not-downloaded -not-listened -not-played -number int -numbers -played -random -shortname string -tag string -this-week -today -url\n")
		fmt.Printf("        lists episodes\n")
		fmt.Printf("  feeds -kind string -random -shortname string -url string\n")
		fmt.Printf("        lists feeds\n")
		fmt.Printf("  import -url string\n")
		fmt.Printf("        imports a feed from a URL\n")
		fmt.Printf("  rename -shortname string -newname string\n")
		fmt.Printf("        changes the shortname of an imported feed from shortname to newname\n")
		fmt.Printf("  update -shortname string\n")
		fmt.Printf("        updates feeds\n")
		os.Exit(1)
	}

	// Get user's home directory
	user, err := user.Current()
	checkError(err, "Could not find home directory.\n")

	// Read the config file
	dat, err := os.ReadFile(filepath.Join(user.HomeDir, ".podcat.json"))
	if err != nil {
		dat, err = os.ReadFile(filepath.Join(user.HomeDir, ".config", "podcat", "podcat.json"))
		checkError(err, "Could not read configuration file.\n")
	}
	var config feedman.Config
	json.Unmarshal(dat, &config)

	// Check that ffmpeg is available
	_, err = exec.LookPath("ffmpeg")
	if err != nil {
		fmt.Printf("'ffmpeg' command not found.\n")
		os.Exit(1)
	}

	// Check that wget is available
	_, err = exec.LookPath("wget")
	if err != nil {
		fmt.Printf("'wget' command not found.\n")
		os.Exit(1)
	}

	// Verify that a subcommand has been provided
	if len(os.Args) < 2 {
		flag.Usage()
	}

	// Switch on subcommand
	switch os.Args[1] {
	case "download":
		commandDownload.Parse(os.Args[2:])
	case "episodes":
		commandEpisodes.Parse(os.Args[2:])
	case "feeds":
		commandFeeds.Parse(os.Args[2:])
	case "import":
		commandImport.Parse(os.Args[2:])
	case "rename":
		commandRename.Parse(os.Args[2:])
	case "update":
		commandUpdate.Parse(os.Args[2:])
	default:
		flag.Usage()
	}

	// Process subcommands
	if commandDownload.Parsed() {
		if maxDownloads > 0 {
			config.MaxDownloads = maxDownloads
		}
		sort.Slice(config.Feeds, func(i, j int) bool {
			return config.Feeds[i].Title < config.Feeds[j].Title
		})
		if shortname == "" {
			// Download episodes for all feeds
			for _, f := range config.Feeds {
				ch := make(chan feedman.Result)
				go config.DownloadEpisodesFromFeed(ch, f.Shortname)
				for r := range ch {
					if r.Err != nil {
						fmt.Println(r.Err)
						os.Exit(1)
					}
					if r.Value != "" {
						fmt.Println(r.Value)
					}
				}
			}
		} else {
			// Download episodes for a specific feed
			ch := make(chan feedman.Result)
			go config.DownloadEpisodesFromFeed(ch, shortname)
			for r := range ch {
				if r.Err != nil {
					fmt.Println(r.Err)
					os.Exit(1)
				}
				if r.Value != "" {
					fmt.Println(r.Value)
				}
			}
		}
	} else if commandEpisodes.Parsed() {
		if epFirst && epLatest {
			fmt.Printf("The -first and -lastest flags are mutually exclusive.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}
		if epListened && epNotListened {
			fmt.Printf("The -listened and -not-listened flags are mutually exclusive.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}
		if epPlayed && epNotPlayed {
			fmt.Printf("The -played and -not-played flags are mutually exclusive.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}
		if epAsc && epDesc {
			fmt.Printf("The -asc and -desc flags are mutually exclusive.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}
		if epRandom && epNumber > 0 {
			fmt.Printf("The -random and -number flags are mutually exclusive.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}
		if epDownloaded && epNotDownloaded {
			fmt.Printf("The -downloaded and -not-downloaded flags are mutually exclusive.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}
		if epListened && epNext {
			fmt.Printf("The -listened flag and the -next flag are mutually exclusive.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}
		if epThisWeek && epToday {
			fmt.Printf("The -this-week flag and the -today flag are mutually exclusive.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}
		if epNumbers && feedname {
			fmt.Printf("The -numbers flag and the -feedname flag are mutually exclusive.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}
		if epKind != "" && epKind != "audio" && epKind != "video" {
			fmt.Printf("The -kind value is invalid.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}

		// Initialise tabwriter
		w := new(tabwriter.Writer)
		w.Init(os.Stdout, 0, 8, 8, '\t', tabwriter.AlignRight)
		defer w.Flush()

		var eps []feedman.Episode
		if shortname != "" {
			eps, err = config.GetEpisodesForFeed(shortname)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		} else {
			if epKind == "" {
				var feeds []feedman.Feed
				if epTag != "" {
					for _, f := range config.Feeds {
						if contains(f.Tags, epTag) {
							feeds = append(feeds, f)
						}
					}
				} else {
					feeds = config.Feeds
				}
				for _, f := range feeds {
					es, err := config.GetEpisodesForFeed(f.Shortname)
					if err != nil {
						fmt.Println(err)
						os.Exit(1)
					}
					eps = append(eps, es...)
				}
			} else {
				filteredFeeds := config.FilterFeedOnKind(epKind)
				var feeds []feedman.Feed
				if epTag != "" {
					for _, f := range filteredFeeds {
						if contains(f.Tags, epTag) {
							feeds = append(feeds, f)
						}
					}
				} else {
					feeds = filteredFeeds
				}
				for _, f := range feeds {
					es, err := config.GetEpisodesForFeed(f.Shortname)
					if err != nil {
						fmt.Println(err)
						os.Exit(1)
					}
					eps = append(eps, es...)
				}
			}
		}

		var timeEps []feedman.Episode
		if epThisWeek {
			tNow := time.Now()
			for _, ep := range eps {
				tPub := time.Unix(ep.Published, 0)
				if tNow.YearDay() <= tPub.YearDay()+7 && tNow.Year() == tPub.Year() {
					timeEps = append(timeEps, ep)
				}
			}
		} else if epToday {
			tNow := time.Now()
			for _, ep := range eps {
				tPub := time.Unix(ep.Published, 0)
				if tNow.YearDay() == tPub.YearDay() && tNow.Year() == tPub.Year() {
					timeEps = append(timeEps, ep)
				}
			}
		} else {
			timeEps = eps
		}

		var filterEps []feedman.Episode
		if epListened {
			filterEps = feedman.FilterEpisodesOnListened(timeEps)
		} else if epNotListened {
			filterEps = feedman.FilterEpisodesOnNotListened(timeEps)
		} else {
			filterEps = timeEps
		}
		if len(filterEps) > 0 {
			var printEps []feedman.Episode
			if epLatest {
				printEps = feedman.GetLatestEpisode(filterEps)
			} else if epFirst {
				printEps = feedman.GetFirstEpisode(filterEps)
			} else if epNext {
				printEps = feedman.GetNextEpisode(filterEps)
			} else if epDownloaded {
				printEps = feedman.GetDownloadedEpisodes(filterEps)
			} else if epNotDownloaded {
				printEps = feedman.GetNotDownloadedEpisodes(filterEps)
			} else {
				printEps = filterEps
			}
			if len(printEps) > 0 {
				if epAsc {
					// Order ascending
					sort.Slice(printEps, func(i, j int) bool {
						return printEps[i].Number < printEps[j].Number
					})
				} else if epDesc {
					// Order descending
					sort.Slice(printEps, func(i, j int) bool {
						return printEps[i].Number > printEps[j].Number
					})
				} else {
					// Order date/time published
					sort.Slice(printEps, func(i, j int) bool {
						return printEps[i].Published > printEps[j].Published
					})
				}
				if epRandom {
					printEps = feedman.SelectRandomEpisode(printEps)
				} else if epNumber > 0 {
					printEps = config.SelectEpisodeByNumber(shortname, epNumber)
				}
				if epPlayed && shortname != "" {
					for i, ep := range printEps {
						config.SetEpisodeAsListened(shortname, ep.Number)
						printEps[i].Listened++
					}
				} else if epNotPlayed && shortname != "" {
					for i, ep := range printEps {
						config.SetEpisodeAsNotListened(shortname, ep.Number)
						printEps[i].Listened--
					}
				}
			}

			for _, ep := range printEps {
				var downloaded string
				if ep.Downloaded {
					downloaded = "Downloaded"
				} else {
					downloaded = "Not Downloaded"
				}
				epName := strings.ReplaceAll(ep.Name, "&#x27;", "'")
				epName = strings.ReplaceAll(epName, "&amp;", "&")
				epName = strings.ReplaceAll(epName, "&quot;", "\"")

				if len(epName) > 80 {
					epName = strings.Join([]string{epName[:80], "..."}, "")
				}

				if epURL {
					// Print episode URL(s)
					fmt.Println(ep.URL)
				} else if epFile {
					// Print episode file(s)
					fmt.Println(ep.File)
				} else if epNumbers {
					// Print epsiode detail(s)
					fmt.Fprintf(w, "%d\t%s\t%d\t%s\n", ep.Number, epName, ep.Listened, downloaded)
				} else if feedname {
					if len(epName) > 50 {
						epName = strings.Join([]string{epName[:50], "..."}, "")
					}
					// Print epsiode detail(s)
					fmt.Fprintf(w, "%d\t%s\t%s\t%d\n", ep.Number, ep.FeedName, epName, ep.Listened)
				} else {
					// Print epsiode detail(s)
					fmt.Fprintf(w, "%s\t%d\t%s\n", epName, ep.Listened, downloaded)
				}
			}
		}
	} else if commandFeeds.Parsed() {
		if feedURL && feedShortname {
			fmt.Printf("The -url and -shortname flags are mutually exclusive.\n\n")
			commandFeeds.Usage()
			os.Exit(1)
		}
		if feedKind != "" && feedKind != "audio" && feedKind != "video" {
			fmt.Printf("The -kind value is invalid.\n")
			commandEpisodes.Usage()
			os.Exit(1)
		}

		// Initialise tabwriter
		w := new(tabwriter.Writer)
		w.Init(os.Stdout, 0, 8, 8, '\t', tabwriter.AlignRight)
		defer w.Flush()

		// Filters
		var printFeeds []feedman.Feed
		if feedKind != "" {
			printFeeds = config.FilterFeedOnKind(feedKind)
		} else {
			for _, f := range config.Feeds {
				printFeeds = append(printFeeds, f)
			}
		}
		// Print each filename/foldername
		if len(printFeeds) > 0 {
			if feedRandom {
				printFeeds = config.SelectRandomFeed(printFeeds)
			}
			// Order descending
			sort.Slice(printFeeds, func(i, j int) bool {
				return printFeeds[i].Shortname < printFeeds[j].Shortname
			})
			for _, f := range printFeeds {
				var numDown int16
				eps, err := config.GetEpisodesForFeed(f.Shortname)
				if err != nil {
					fmt.Println(err)
				}
				for _, ep := range eps {
					if ep.Downloaded {
						numDown++
					}
				}
				if feedShortname {
					fmt.Println(f.Shortname)
				} else if feedURL {
					fmt.Println(f.URL)
				} else {
					fmt.Fprintf(w, "%s\t%d\t%s\n",
						f.Title, numDown, f.Shortname)
				}
			}
		}
	} else if commandImport.Parsed() {
		if importURL == "" {
			// Import all the feeds in config file
			res, err := config.ImportFeeds()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			if len(res) > 0 {
				for _, i := range res {
					fmt.Println(i)
				}
			}
		} else {
			// Import a specific feed from a URL
			res, err := config.ImportFeed(importURL, 0)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			if res != "" {
				fmt.Println(res)
			}
		}
	} else if commandRename.Parsed() {
		if shortname != "" && newname != "" {
			err := config.Rename(shortname, newname)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		} else if shortname != "" && newTitle != "" {
			err := config.RenameTitle(shortname, newTitle)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		} else {
			commandRename.Usage()
			os.Exit(1)
		}
	} else if commandUpdate.Parsed() {
		sort.Slice(config.Feeds, func(i, j int) bool {
			return config.Feeds[i].Title < config.Feeds[j].Title
		})
		if shortname == "" {
			// Update all feeds
			for _, f := range config.Feeds {
				ch := make(chan feedman.Result)
				go config.UpdateFeed(ch, f.Shortname)
				for res := range ch {
					if res.Err != nil {
						fmt.Println(res.Err)
						if !strings.Contains(res.Err.Error(), "5") {
							os.Exit(1)
						}
					}
					if res.Value != "" {
						fmt.Println(res.Value)
					}
				}
			}
		} else {
			// Update a specific feed
			ch := make(chan feedman.Result)
			go config.UpdateFeed(ch, shortname)
			for res := range ch {
				if res.Err != nil {
					fmt.Println(res.Err)
					os.Exit(1)
				}
				if res.Value != "" {
					fmt.Println(res.Value)
				}
			}
		}
	}
}

// checkErrors check for an error and, if found, exit
func checkError(err error, message string) {
	if err != nil {
		fmt.Printf(message)
		os.Exit(1)
	}
}

// contains checks if a slice contains a particular string
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
